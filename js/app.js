"use strict";
(function () {
	//Variáveis Globais;
	var evento, inicio, sobre, local, organizacao, programacao, patrocinio;

	/**
	 *	Esta função preenche as telas do app com conteúdo do evento
	 */
	var fill = function() {
		inicio = getById("inicio");
		sobre = getById("sobre");
		local = getById("local");
		organizacao = getById("organizacao");
		programacao = getById("programacao");
		patrocinio = getById("patrocinio");

		//Preenche o conteúdo de cada seção
		fillSobre();
		fillLocal();
		fillProgramacao();
		fillOrganizacao();
		fillPatrocinio();

		//Configura o início do app
		changeView(inicio);
		addEventListeners();
		setLinks();
	};

	/**
	 * Preenche a view Sobre com conteúdo
	 */
	var fillSobre = function() {
		getById("nomeEvento").textContent = evento.nome;
		getById("descricao").innerHTML = evento.descricao;
		document.querySelector(".saudacao .icon-arrow-right").style.display = "block";
	}

	/**
	 * Preenche a view Localização com conteúdo
	 */
	var fillLocal = function() {
		//Mostrando endereço
		var nome = fillElm('h2', evento.local.nome, null, null),
			endereco = fillElm('p', evento.local.endereco, null, null);

		elmAppend(local, nome);
		elmAppend(local, endereco);
	}

	/**
	 * Inicializa o Canvas do Google Maps com Latitude e Longitude do Local do Evento
	 */
	var initMaps = function(){

		//Maps
		elmSetStyle(getById('map-canvas'),"display", "block");

		var myLatlng = new google.maps.LatLng(evento.local.lat, evento.local.long);
		var mapOptions = {
			zoom: 15,
			center: myLatlng
		}
		var map = new google.maps.Map(getById('map-canvas'), mapOptions);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title: evento.local.nome
		});
	}

	/**
	 * Preenche a view Programação com conteúdo
	 */
	var fillProgramacao = function() {
		if(evento.programacao.length === 0){
			programacao.innerHTML = "Grade de programação ainda não disponível";
		} else {

			//Agrupando os dias do evento;
			var dias = groupArray(evento.programacao, "inicio", true, "", "dia");

			//Listando palestras por Dia
			for(var i in dias){
				var dia = fillElm('h2', i, "dia-prog", null);
				elmAppend(programacao, dia);

				//Agrupando os horários;
				var horas = groupArray(dias[i], "inicio", true, "", "hora");

				//Listando palestras por Horário
				for(var i in horas){
					var hora = fillElm('h3', i, null, null),
						list = fillElm('ul', null, "lista-palestrantes", null);

					for(var j in horas[i]){
				        var item = setElm('li');

				        var sala = fillElm('span', 
				        	"["+getDateInfo(horas[i][j].inicio, "hora")+" - "+getDateInfo(horas[i][j].fim, "hora")+"] "
				        	+ horas[i][j].sala, "sala", null);
				        var palestrante = fillElm('span', horas[i][j].palestrante, "palestrante", null);
				        var titulo = fillElm('span', horas[i][j].titulo, "titulo", null);
				        var descricao = fillElm('span', horas[i][j].descricao, "descricao", null);
				        
				        // Setando o conteúdo da palestra;
				        elmAppend(item, [sala, palestrante, titulo, descricao]);

				        // Adicionando a palestra na lista
				        elmAppend(list, item);
			    	}

			    	//Adicionando as palestras de um horário específico
					elmAppend(programacao, [hora, list]);
				}
			}
		}
	}

	/**
	 * Preenche o conteúdo da view Organização
	 */
	var fillOrganizacao = function() {

		//Agrupando os organizadores;
		var orgs = groupArray(evento.organizacao, "grupo", true, "Geral");

		//Listando por Grupo
		for(var i in orgs){
			var grupo = fillElm('h2', ((i !== "Geral") ? "GT - "+i : i), null, null),
				list = setElm('ul');

			for(var j in orgs[i]){
		        var item = setElm('li');

		        var nome = fillElm('span', orgs[i][j].nome+" - ", null, null);
		        var email = fillElm('a', orgs[i][j].email, null, "mailto:"+orgs[i][j].email);
		        
		        // Adicionando os dados de um colaborador
		        elmAppend(item, [nome, email, grupo]);

		        // Adicionando o colaborador na lista
		        elmAppend(list, item);
	    	}
	    	//Adicionando um GT na view
			elmAppend(organizacao, [grupo, list]);
		}
	}

	/**
	 * Preenche o conteúdo da view Patrocínio
	 */
	var fillPatrocinio = function() {
		//Agrupando os patrocinadores por tipo de patrocinio, 
		var patrs = groupArray(evento.patrocinio, "tipo", false),
			tipos = ["diamante", "ouro", "prata", "apoio", "realizacao"];

		//Listando por tipo de patrocinio
		for(var i in tipos){
			var tipo = fillElm('h2', ucfirst(tipos[i]), null, null),
				list = setElm('ul');

			if(patrs[tipos[i]] === undefined){
				//reaproveitando variável
				list = fillElm('span', null, "nopatr", null);

		        var novo = fillElm('a', "Seja o primeiro!!!", null, evento.chamadapatrocinio);
			    elmAppend(list, novo);
			} else {
				for(var j in patrs[tipos[i]]){
					var patrocinador = patrs[tipos[i]][j];

			        var item = setElm('li');

			        var nome = fillElm('span', patrocinador.nome, "nome", null);
			        var url = fillElm('span', patrocinador.site, "url", null);
			        var imglogo = setElm('img');
			        imglogo.src = patrocinador.logo;
			        var website = fillElm('a', null, null, patrocinador.site);

			        elmAppend(website, [imglogo, nome, url]);
			        elmAppend(item, website);
			        elmAppend(list, item);
			    }
			}

			elmAppend(patrocinio, [tipo, list]);
		}
	}

	/**
	 *	Troca a view atual do app
	 */
	var changeView = function (view) {
		setTitulo(view.id);
		var views = document.getElementsByClassName("view");
		esconder(views);
		elmSetStyle(view, "display", "block");
		elmClass(getByTag("body")[0], "");
		if(view.id === "local") {
			initMaps();
		}
	}

	/**
	 *	Configura o título conforme a view ativa
	 */
	var setTitulo = function (view){
		var titulo = getById("titleView");
		switch(view){
			case "sobre":
				titulo.textContent = "Sobre";
				break;
			case "local":
				titulo.textContent = "Localização";
				break;
			case "programacao":
				titulo.textContent = "Programação";
				break;
			case "organizacao":
				titulo.textContent = "Organizadores";
				break;
			case "patrocinio":
				titulo.textContent = "Patrocinadores";
				break;
			default:
				titulo.textContent = "FGSL";
				break;
		}
	}

	/**
	 *	Busca todas as tags a e seta seus respectivos href
	 */
	var setLinks = function (){
		var links = getByTag('a');
		for (var i = links.length - 1; i >= 0; i--) {
			switch(links[i].getAttribute("class")) {
				case "twitter":
					links[i].href = evento.twitter;
					break;
				case "facebook":
					links[i].href = evento.facebook;
					break;
				case "website":
					links[i].href= evento.website;
					break;
				case "changeView":
					try {
						links[i].addEventListener("click", function(e){
							e.preventDefault();
							var urlElems = this.href.split("/");
							urlElems = urlElems[urlElems.length-1];
							changeView(getById(urlElems));
						});
					}
					catch(e){
						continue;
					}
					break;
				default:
					//não faz nada :)
					break;
			}
		}
	}

	/**
	 *	Esconde algum elemento
	 */
	var esconder = function(elements) {
		for(var i in elements){
			try {
				elmSetStyle(elements[i], "display", "none");
			}
			catch(e){
				continue;
			}
		}
	};

	/**
	 *	Possiveis ações a ser setadas nos links da função addEventListeners();
	 */
	var acoes = {
		menu : function(){
			changeView(sobre);
		},
		toggleMenu : function(){
			var body = getByTag("body")[0],
			className = "menu-active";

			if (body.classList) {
			  body.classList.toggle(className);
			} else {
			  var classes = body.className.split(' ');
			  var existingIndex = classes.indexOf(className);

			  if (existingIndex >= 0)
			    classes.splice(existingIndex, 1);
			  else
			    classes.push(className);

			  body.className = classes.join(' ');
			}
		}
	}

	/**
	 *	Esconde algum elemento
	 */
	var addEventListeners = function(){
		var elems = document.getElementsByClassName("acao");
		for(var i = 0; i < elems.length;i++){
			try {
				elems[i].addEventListener("click", function(e){
					e.preventDefault();
					var urlElems = this.href.split("/");
					urlElems = urlElems[urlElems.length-1];
					acoes[urlElems]();
				});
			}
			catch(e){
				continue;
			}
		}
	};

	/**
	 *	Retorna um novo array agrupando os elementos do array passado em parâmetro:
	 *	arr - Array a ser agrupado
	 *  grupo - Índice de arr pelo qual será agrupado
	 *  sort - booleano, se o array será ordenado ou não
	 *  main - Caso o array seja ordenado, qual será o grupo em destaque
	 *  tpdata - (Em caso de datas) se agrupará em hora ou dia
	 */
	var groupArray = function(arr, grupo, sort, main, tpdata) {
		var max = 0;
		var group = [], grp = [];
		for (var i = arr.length; --i >= 0;) {
			switch(tpdata) {
				case "hora":
					arr[i][tpdata] = getDateInfo(arr[i][grupo], "hora");
					break;
				case "dia":
					arr[i][tpdata] = getDateInfo(arr[i][grupo], "dia");
					break;
				default:
					tpdata = grupo;
					break;
			}
		  var value = arr[i][tpdata];
		  if (group[value] === undefined){
		  	group[value] = [];
		  }
		  group[value].push(arr[i]);
		}
		if (sort){
			var k = Object.keys(group);
			k = k.sort();
			if(main !== undefined && main !== ""){
				grp[main] = group[main];
			}
			for(var i in k){
				if (k[i] !== main)
					grp[k[i]] = group[k[i]];
			}
		} else {
			grp = group;
		}
		return grp;
	}

	/**
	 *	Retorna alguma informação de uma data (Hora ou Dia/mes)
	 */
	var getDateInfo = function (data, info) {
		switch(info) {
			case "hora":
				var dt = new Date(data);
				return ((dt.getHours()<10?'0':'') + dt.getHours() )+":"+((dt.getMinutes()<10?'0':'') + dt.getMinutes() )
				break;
			case "dia":
				var dt = new Date(data),
					dia = ((dt.getDate()<10?'0':'') + dt.getDate()),
					mes = ((dt.getMonth()+1<10?'0':'') + (dt.getMonth()+1))
				return dia+"/"+mes;
				break;
			default:
				return new Date(data);
		}
	}

	/**
	 *	Obtem os dados do evento via ajax
	 */
	var getDados = function(e, url, err) {		
		var request = new XMLHttpRequest();
		if (url === undefined){
			url = "http://fgsl.ledlabs.com.br/evento.php";
		}
		request.open('GET', url, true);
		request.onreadystatechange = function() {
		  if (this.readyState === 4){
		    if (this.status >= 200 && this.status < 400){
		      evento = JSON.parse(this.responseText).evento;
		      fill();
		    } else {
		      if(!err)
		      {
		        getDados(null, "evento.json", true);
		      } else {
				getById("nomeEvento").textContent = "Falha ao Obter dados =(";
		      }
		    }
		  }
		};

		request.send();
		request = null;
	};

	//Funções utilitárias
	var getById = function(id) {
		return document.getElementById(id);
	};

	var getByTag = function(tag) {
		return document.getElementsByTagName(tag);
	};

	var setElm = function(elm) {
		return document.createElement(elm);
	};

	var elmClass = function(elm, cls) {
		elm.className = cls;
	};

	var fillElm = function(elm, text, cls, href) {
	    elm = setElm(elm);
	    elm.textContent = text;
	    elmClass(elm, cls);
	    elm.href = href;
	    return elm;
	};

	var elmAppend = function(elm, child) {
		if (Array.isArray(child)) {
			for (var i in child) {
				elm.appendChild(child[i]);
			}
		} else {
			elm.appendChild(child);
		}
	};

	var elmSetStyle = function(elm, style, value) {
		elm.style[style] = value;
	};

	var ucfirst = function(text) {
		return text[0].toUpperCase() + text.slice(1);
	};

	//inicializando app
	window.addEventListener("load", getDados);
})();