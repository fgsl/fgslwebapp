FgslWebApp
==========

Este projeto visa construção de uma web app do Evento FGSL (Fórum Goiano de Software Livre) http://fgsl.net

Ainda está em fase de desenvolvimento, obtenha na loja que mais combina com você:

* Play Store - https://play.google.com/store/apps/details?id=br.org.aslgo.fgsl

* Firefox MarketPlace - https://marketplace.firefox.com/app/fgsl/

* Chrome Webstore - https://chrome.google.com/webstore/detail/fgsl/ghihhfoahkdmfpfjapbnnfheiklfbojm?hl=pt-BR

* Web - http://fgsl.ledlabs.com.br


=Atualizacao=

A atualização dos dados é praticamente toda feita através da edição do arquivo "evento.json". Você pode perceber que, no arquivo "js/app.js" é requisitado uma url(http://fgsl.ledlabs.com.br/evento.php). Este arquivo PHP nada mais é do que um hack para burlar o CORS do github. Não foi adicionado neste repositório pois não tem relevância para o projeto. Você pode substituí-lo pela chamada do evento.json (localmente) ou criar o seu próprio evento.php:

	<?php
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	header('Access-Control-Allow-Methods: GET, POST, PUT');
	header('Content-type: application/json; charset=UTF-8');

	$json = file_get_contents("https://raw.githubusercontent.com/sheldonled/FgslWebApp/master/evento.json");

	echo $json;